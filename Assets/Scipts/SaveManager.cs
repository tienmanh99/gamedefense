using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System;

public class SaveManager : MonoBehaviour
{
    public Action OnMapGenerate;
    public List<Vector3> cachedPath;
    public GenMap genmap;
    public string mapName;
    public void Init()
    {
        OnMapGenerate += OnGenMapCall;
    }
    public void OnGenMapCall()
    {
        var save = new SaveMap();

        save.width = genmap.widthmap;
        save.heigh = genmap.heighmap;
        save.pathTile = cachedPath;
        save.name = mapName;
        string datastr = JsonUtility.ToJson(save);
        PlayerPrefs.SetString("genmap", datastr);
    }
    public void Save()
    {
        var save = new SaveMap();
        save.width = genmap.widthmap;
        save.heigh = genmap.heighmap;
        save.pathTile = cachedPath;
        save.name = mapName;

        string maplist = PlayerPrefs.GetString("genmaplist", "{}");

        if (PlayerPrefs.HasKey("genmap"))
        {
            var lib = PlayerPrefs.GetString("genmap");
            
            GenMapList libData = JsonUtility.FromJson<GenMapList>(maplist);
            
            libData.library.Add(save);

      
            var newJsonData = JsonUtility.ToJson(libData);
           
            PlayerPrefs.SetString("genmaplist", newJsonData);
            PlayerPrefs.Save();
          

        }
        
    }
    public void Load(string mapID) 
    {

        if (PlayerPrefs.HasKey("genmaplist"))
        {
            var lib = PlayerPrefs.GetString("genmaplist");
         
            GenMapList libData = JsonUtility.FromJson<GenMapList>(lib);     
            foreach (var map in libData.library)
            {
                if (map.name == mapID)
                {
                    LoadMap(map);
                    break;
                }
                else
                {
                    GenerateMap();
                    break;
                }
            }
        }
      

    }

    private void OnDestroy()
    {
        OnMapGenerate -= OnGenMapCall;
    }
    protected void LoadMap(SaveMap map)
    {

        int y = 0;
        for (int x = 0; x < map.heigh; x += 4)
        {
            for (int z = 0; z < map.width; z += 4)
            {

                GameObject newMap = (GameObject)Instantiate(genmap.Map);
                genmap.maptiles.Add(newMap);
                newMap.transform.position = new Vector3(x, y, z);
                
            }
        }
        genmap.startTile.transform.position = map.pathTile[0];
        genmap.endTile.transform.position = map.pathTile[map.pathTile.Count - 1];

        foreach (var item in map.pathTile)
        {
            foreach (var tile in genmap.maptiles)
            {
                
                if (item.x == tile.transform.position.x && item.y == tile.transform.position.y && item.z == tile.transform.position.z)
                {
                    GameObject pathlane = Instantiate(genmap.wayPrefabs, tile.transform.position, Quaternion.identity);
                    GameObject pathLane = Instantiate(genmap.waypoints, tile.transform.position + genmap.setup, Quaternion.identity);
                    genmap.waylane.Add(pathLane);
                }

            }
        }
        GameObject str = Instantiate(genmap.SpawnPoint, genmap.startTile.transform.position + genmap.offSet, Quaternion.identity);
        GameObject end = Instantiate(genmap.Endpoind, genmap.endTile.transform.position + genmap.offSet, Quaternion.identity);
        
     

    }
    protected void GenerateMap()
    {
        OnMapGenerate += OnGenMapCall;
        genmap.genMap();
        Save();
    }
}
[System.Serializable]
public class SaveMap
{
    public string name;
    public int width, heigh;
    public List<Vector3> pathTile;

}

[System.Serializable]
public class GenMapList
{
    public List<SaveMap> library = new List<SaveMap>();
}
    



