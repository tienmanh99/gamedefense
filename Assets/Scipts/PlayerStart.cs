using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStart : MonoBehaviour
{
    public static int Money;
    public int startMoney = 400;

    public static int Lives;
    public int startLives = 10;
    public static int Rounds;
    public int startRound = 1;

    void Start()
    {
        Money = startMoney;
        Lives = startLives;
        
        Rounds = startRound;
    }
    private void Update()
    {
        if (Lives < 0)
        {
            Lives = 0;
        }
    }

}
