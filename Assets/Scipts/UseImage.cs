using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UseImage : MonoBehaviour
{
    public List<ChangeSprite> data;
    public Mesh sword;
    public Material mat;
    public ItemStat mainItem;
    public int ID;
    private MeshFilter mainMeshFilter;

    private void Start()
    {
        mainMeshFilter = GetComponent<MeshFilter>();
    }

    public void ChangeSprite()
    {
        foreach (var item in data)
        {
            if(item.ID == ID)
            {
                sword = item.swordTochange;
                mat = item.material;
                mainItem = item.item;
                
            }
        }

        if(sword != null)
        {
            mainMeshFilter.mesh = sword;
        }
        Debug.Log(mainItem.damge);
        Debug.Log(sword);
    }
  
}



