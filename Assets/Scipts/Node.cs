using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Node : MonoBehaviour
{
    public Color hoverColor;
    BuildManager buildmanager;
    private Renderer rend;
    private Color startColor;
    public bool isTouchAllow;
    [HideInInspector]
    public GameObject turret;
    [HideInInspector]
    public TurretBlueprint turretBlueprint;
    [HideInInspector]
    public bool IsUpdated = false;
    public bool isTouchDown;


    public Color colorEnoughMoney;
    public Vector3 positionOffest;

    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        buildmanager = BuildManager.instance;
    }

    public static bool IsPointerOverUI(string tag)
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.GetTouch(0).position;
        List<RaycastResult> raysastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raysastResults);
        foreach (RaycastResult raysastResult in raysastResults)
        {
            if (raysastResult.gameObject.CompareTag(tag))
            {
                return true;
            }
        }
        return false;
    }

    void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        if (!buildmanager.CanBuild)
            return;
        if (buildmanager.HaveMoney)
        {
            rend.material.color = hoverColor;
        }
        else
        {
            rend.material.color = colorEnoughMoney;
        }
        
    }
    void OnMouseExit()
    {
        rend.material.color = startColor;
        isTouchDown = false;
    }
    void OnMouseDown()
    {
        //bool v = IsPointerOverUI("UI");
        //if (v)
        //    return;
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        
        if (turret != null)
        {
            buildmanager.SelectNode(this);
            return;
        }
        if (!buildmanager.CanBuild)
            return;
        if (isTouchDown)
        {
            BuildTurret(buildmanager.GetTurretTobuild());
            buildmanager.isChoosed = true;
        }
        
        isTouchDown = true;
        
    }
    void BuildTurret(TurretBlueprint bluePrint)
    {
        if (PlayerStart.Money < bluePrint.cost)
        {
            
            return;
        }
        PlayerStart.Money -= bluePrint.cost;

        // creat turret new
        GameObject _turret = (GameObject)Instantiate(bluePrint.prefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;
        turretBlueprint = bluePrint;
        GameObject effect = (GameObject)Instantiate(buildmanager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 2f);
        isTouchDown = false;
     
    }
    public void UpgradeTurret()
    {
        if (PlayerStart.Money < turretBlueprint.Updatecost)
        {
      
            return;
        }
        PlayerStart.Money -= turretBlueprint.Updatecost;
        Destroy(turret);
        GameObject _turret = (GameObject)Instantiate(turretBlueprint.Upgrateprefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;
        GameObject effect = (GameObject)Instantiate(buildmanager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 2f);
        IsUpdated = true;
       
    }
    public void SellTurret()
    {
        PlayerStart.Money += turretBlueprint.GetSellAmount();
        Destroy(turret);
        turretBlueprint = null;
        IsUpdated = false;


    }
    public Vector3 GetBuildPosition()
    {
        return transform.position + positionOffest;
    }
    
}
