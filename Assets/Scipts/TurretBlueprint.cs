using System.Collections;
using UnityEngine;

[System.Serializable]
public class TurretBlueprint
{
    [Header("Standard")]
    public GameObject prefab;
    public int cost;
    public float range;

    [Header("Upgrade")]
    public GameObject Upgrateprefab;
    public int Updatecost;
    public float upRange;

    public int GetSellAmount()
    {
        
        return cost / 2; 
    }
}
