using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "NewImage", menuName = "ImageToChange")]
public class ChangeSprite : ScriptableObject
{
    public int ID;
    public Mesh swordTochange;
    public Material material;
    public ItemStat item;
        
}
[System.Serializable]
public class ItemStat
{
    public int damge;
    public int magic;
}
