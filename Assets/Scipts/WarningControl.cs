using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WarningControl : MonoBehaviour
{
    public Image img;
    public CameraShanke shanke;
    // Update is called once per frame
    private void Update()
    {
        if(EnemyGo.curLive < EnemyGo.preLive)
        {
            WarningEffect();
            StartCoroutine(shanke.ShankeOnce(0.25f, 0.25f));
        }
    }
    public void WarningEffect()
    {
        StartCoroutine(CountEffect());
        EnemyGo.preLive = EnemyGo.curLive;
    }
    IEnumerator CountEffect()
    {
        img.enabled = true;
        yield return new WaitForSeconds(.4f);
        img.enabled = false;
    }
}
