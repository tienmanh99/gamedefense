using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class SafeArea : MonoBehaviour
{
    private void Awake()
    {
        var rectTranform = GetComponent<RectTransform>();
        var safeArea = Screen.safeArea;
        var anchorMin = safeArea.position;
        var anchoMax = anchorMin + safeArea.size;

        anchorMin.x /= Screen.width;
        anchorMin.y /= Screen.height;
        anchoMax.x /= Screen.width;
        anchoMax.y /= Screen.height;

        rectTranform.anchorMin = anchorMin;
        rectTranform.anchorMax = anchoMax;
    }
}
