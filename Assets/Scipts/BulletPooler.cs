using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPooler : MonoBehaviour
{
    public static BulletPooler instance;
    public List<PoolItem> itemsToPool;
    public List<GameObject> pooldedObjs;
   
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        pooldedObjs = new List<GameObject>();

        foreach (PoolItem item in itemsToPool)
        {
            for (int i = 0; i < item.amountPool; i++)
            {
                GameObject obj = (GameObject)Instantiate(item.objToPool);
                obj.SetActive(false);
                pooldedObjs.Add(obj);
            }
        }
    }
    public GameObject GetPooledObj(string tag)
    {
        for (int i = 0; i < pooldedObjs.Count; i++)
        {
            if (!pooldedObjs[i].activeInHierarchy && pooldedObjs[i].tag == tag)
            {
                return pooldedObjs[i];
            }
        }
        foreach (PoolItem item in itemsToPool)
        {
            if (item.objToPool.tag == tag)
            {
                if (item.isExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.objToPool);
                    obj.SetActive(false);
                    pooldedObjs.Add(obj);
                    return obj;
                }
            }
        }
        return null;

    }


}
[System.Serializable]
public class PoolItem
{
    public GameObject objToPool;
    public int amountPool;
    public bool isExpand = true;
}
  
