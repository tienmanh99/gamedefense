using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    // Start is called before the first frame update
    public CharInfo character;
    public Text name;
    public Text description;
    public Text range;
    public Text damge;
    public Text speedAttack;
    public Image charSprite;

    private void Start()
    {
        name.text = character.name;
        description.text = character.description;
        range.text = character.range.ToString();
        damge.text = character.damge.ToString();
        speedAttack.text = character.speedAttack.ToString();
        charSprite.sprite = character.character;

    }

}
