using UnityEngine;

public class BuildManager : MonoBehaviour
{ 
    public static BuildManager instance;

    private const float TIME_DOUBLE_TOUCH = .2f;
    private float lastTimeClick;
    public bool isChoosed = false;

    public GameObject buildEffect;
    public TurretBlueprint turretToBuild;
    private Node target;
    public NodeUI nodeUI;
    void Awake()
    {
        if(instance != null)
        {
            return;
        }
        instance = this;
    }
    private void Update()
    {
        if (isChoosed == true)
        {
            turretToBuild = null;
        }
        if (Input.GetMouseButtonDown(0))
        {
            float timeBetweenClick = Time.time - lastTimeClick;
            if (timeBetweenClick <= TIME_DOUBLE_TOUCH)
            {
                DeselectNode();
                turretToBuild = null;
          
            }
            lastTimeClick = Time.time;
        }
    }

    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HaveMoney { get { return PlayerStart.Money >= turretToBuild.cost; } }

    public void SelectNode(Node node)
    {
        if(target == node && target.turret != null)
        {
            DeselectNode();
            return;
        }
        target = node;
        
        nodeUI.SetTarget(node);
    }

    public void DeselectNode()
    {
        target = null;
        nodeUI.Hide();
    }
    public void SelectTurretToBuild(TurretBlueprint turret)
    {
        turretToBuild = turret;
        isChoosed = false;
        DeselectNode();
     
    }
    public TurretBlueprint GetTurretTobuild()
    {
        return turretToBuild;
    }
}
