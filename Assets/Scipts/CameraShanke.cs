using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShanke : MonoBehaviour
{
    public IEnumerator ShankeOnce(float time, float magntitude)
    {
        Vector3 originPos = transform.position;
        float totalTime = 0f;
        while(totalTime < time)
        {
            totalTime += Time.deltaTime;
            float x = Random.Range(-1, 1) * magntitude;
            float y = Random.Range(-1, 1) * magntitude;
            float z = Random.Range(-1, 1) * magntitude;

            transform.position += new Vector3(x, y, z);
            yield return null;
        }
        transform.position = originPos;  
    }
}
