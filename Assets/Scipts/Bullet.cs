using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bullet : MonoBehaviour
{
    private Transform target;
    public float speed = 70f;
    public float damage;
    public float explodeRadiuos = 0f;
    public GameObject impactEffect;

    // find target;
    public void Seak(Transform _target)
    {
        target = _target;
    }
    

    // Update is called once per frame
    void Update()
    {
        if(target == null)
        {
            gameObject.SetActive(false);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if(dir.magnitude <= distanceThisFrame)
        {
            Hittarget();
            return;
        }
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
           
    }
 
    void Hittarget()
    {
        GameObject impactEff = (GameObject)Instantiate(impactEffect);
        impactEff.transform.position = transform.position;
        impactEff.transform.localEulerAngles = new Vector3(transform.eulerAngles.x - 90, transform.eulerAngles.y, transform.eulerAngles.z);
        Destroy(impactEff, 2f);
        if (explodeRadiuos > 0f)
        {
            Explode();
        }
        else
        {
            DamageEnemy(target);
        }

        gameObject.SetActive(false);
    }

    // tao range va gay damage theo range
    //tim cach bo collider
    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explodeRadiuos);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                DamageEnemy(collider.transform);
            }

        }
    }
    void DamageEnemy(Transform Enemy)
    {
        Enemy e = Enemy.GetComponent<Enemy>();
        e.TakeDamge(damage);
    }
    //void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireSphere(transform.position, explodeRadiuos);
    //}
}
