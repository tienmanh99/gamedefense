using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEnemy : MonoBehaviour
{
    private int armor = 2;
    private int magicDef = 2;
    public bool isPhysicdamge;
   
    private Characters character;

    private void Start()
    {
        //Typedamge randomTypeDam = (Typedamge)Random.Range(0, System.Enum.GetValues(typeof(Typedamge)).Length);
        Typedamge typeDamage = Typedamge.PHYSIC;
        character = new Characters(typeDamage);
        Debug.Log(character.TypeDam);
    }
    private void Update()
    {
        GetReceivedDamage();
    }
    void GetReceivedDamage()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            CheckTypeOfDamge(character.TypeDam);
        }
    }
    //kiem tra damge nhan vao
    void CheckTypeOfDamge(Typedamge typeDamage)
    {
        switch (typeDamage)
        {
            case Typedamge.PHYSIC:
                PhysicDamgeCal(character.Damage);
                break;
            case Typedamge.MAGIC:
                MagicDamgeCal(character.Damage);
                break;
            case Typedamge.SHADOW:
                ShadowDamgeCal(character.Damage);
                break;
            case Typedamge.LIGHT:
                LightDamgeCal(character.Damage);
                break;

        }
    }
    private void LightDamgeCal(int damage)
    {
        throw new System.NotImplementedException();
    }
    private void ShadowDamgeCal(int damage)
    {
        throw new System.NotImplementedException();
    }
    float MagicDamgeCal(float damge)
    {
        damge -= magicDef;
        return damge;
    }
    int PhysicDamgeCal(int damge)
    {
        damge -= armor;
        return damge;
    }
    //tru di giapbuffthem get damge return damge
    void DamgeCalculor(int lastdamge)
    {
        if (lastdamge <= 0) return;
        //curHealth -= lastdamge;
    }
}
public enum Typedamge
{
    PHYSIC,
    MAGIC,
    SHADOW,
    LIGHT
}
public class Characters
{
    private int health;
    private int armor;
    private int magicdef;
    private int damageReduction;
    private float speed;
    private int damage;
    private Typedamge typeDam;
    private int range;
    private int mana;
    private int attackrate;
    public Characters(Typedamge typeDamge)
    {
        this.typeDam = typeDamge;
    }
    public Typedamge TypeDam { get => typeDam; }
    public int Damage { get => damage; }
}

