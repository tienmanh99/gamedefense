using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public TurretBlueprint standardTurret;
    public TurretBlueprint missileLauncher;
    public TurretBlueprint laserBeamer;
    

    BuildManager buildmanager;
    void Start()
    {
        buildmanager = BuildManager.instance;    
    }
    
    public void SelectStandardTurret()
    {
        buildmanager.SelectTurretToBuild(standardTurret);
   
    
    }
    public void SelectMissileTurret()
    {
        buildmanager.SelectTurretToBuild(missileLauncher);
   
      
    }
    public void SelectLaserTurret()
    {
        buildmanager.SelectTurretToBuild(laserBeamer);
   
    }
}
