using UnityEngine.UI;
using UnityEngine;

public class NodeUI : MonoBehaviour
{
    private Node target;
    public GameObject UI;
    public SpriteRenderer range;
    public Text UpgradeCost;
    public Button upgradeButton;
    public Text sellcost;
    private Vector3 setRangePos;

    private void Start()
    {
        range.size = new Vector3(10f, 10f, 10f);
    }
    public void SetTarget(Node _target)
    {
        target = _target;
        transform.position = target.GetBuildPosition();
        setRangePos = target.GetBuildPosition() + new Vector3(0f, 0.5f, 0f);
        if (target.IsUpdated)
        {
            float rangeScale = target.turretBlueprint.upRange* 2;
            range.transform.position = setRangePos;
            range.transform.localScale = new Vector3(rangeScale, rangeScale, rangeScale);
        }
        else
        {
            float rangeScale = target.turretBlueprint.range * 2;
            range.transform.position = setRangePos;
            range.transform.localScale = new Vector3(rangeScale, rangeScale, rangeScale);
        }
        if (!target.IsUpdated)
        {
            UpgradeCost.text = "$" + target.turretBlueprint.Updatecost.ToString();
            upgradeButton.interactable = true;
        }
        else
        {
            UpgradeCost.text = "Done!";
            upgradeButton.interactable = false;
        }
        sellcost.text = "$" + target.turretBlueprint.GetSellAmount();
        UI.SetActive(true);
        range.enabled = true;
    }
    public void Hide()
    {
        UI.SetActive(false);
        range.enabled = false;
    }
    public void Upgrade()
    {
        target.UpgradeTurret();
        BuildManager.instance.DeselectNode();
    }
    public void Sell()
    {
        target.SellTurret();
        BuildManager.instance.DeselectNode();
    }
}
