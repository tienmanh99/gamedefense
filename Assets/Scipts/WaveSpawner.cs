using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class WaveSpawner : MonoBehaviour
{
    public static int EnemyAlives;
    public float timeBettwenWave;
    public Text round;
    public Text waveTimeBettwen;
    public ParticleSystem spawnEnemyEffect;
    private ParticleSystem effectEnemySpawn;
    private float timeCount = 5f;
    private int waveIndex = 0;
    public GameObject crossRoundUI;
    public Text roundCross;
    public Gamemanager gamemanager;
    public List<Wave> waves;
    
    [HideInInspector]public List<GameObject> pooledEnemies;
    
    private void Awake()
    {
        Enemy e = GetComponent<Enemy>();

    }
    void Start()
    {
        effectEnemySpawn = Instantiate(spawnEnemyEffect);

        effectEnemySpawn.Stop();
        EnemyAlives = 0;
        round.text = "ROUND: 1/" + waves.Count;
        pooledEnemies = new List<GameObject>();
        foreach (Wave wave in waves)
        {
            for (int i = 0; i < wave.count; i++)
            {
                GameObject obj = Instantiate(wave.enemy);
                obj.SetActive(false);
                pooledEnemies.Add(obj);
            }

        }     
    }
    private void Update()
    {
        
        if (EnemyAlives > 0)
        {
            return;
        }
        int rounds = PlayerStart.Rounds + 1;
        if(rounds == waves.Count)
        {
            roundCross.text = "FINAL ROUND";
        }
        else
        {
            roundCross.text = "ROUND " + rounds;
        }
        
        if (waveIndex == waves.Count && PlayerStart.Lives > 0 && EnemyAlives <= 0)
        {
            gamemanager.Winlevel();
            this.enabled = false;
        }
        if (PlayerStart.Lives <= 0)
        {
            gamemanager.EndLevel();
            this.enabled = false;
        }
        if (timeCount == 0f)
        {
            crossRoundUI.SetActive(true);
            StartCoroutine(SpawnWave());
            timeCount = timeBettwenWave;
            return;
        }
        crossRoundUI.SetActive(false);
        timeCount -= Time.deltaTime;
        timeCount = Mathf.Clamp(timeCount, 0f, Mathf.Infinity);
        waveTimeBettwen.text = string.Format("{0:00.00}", timeCount);
    }  
    IEnumerator SpawnWave()
    {
 
        PlayerStart.Rounds++;
        Wave wave = waves[waveIndex];
        EnemyAlives = wave.count;
        Debug.Log(EnemyAlives);
        for (int i = 0; i < wave.count; i++)
        {
            SpawnEnemy();
            SpawnEffect();

            yield return new WaitForSeconds(01f / wave.rate );
            effectEnemySpawn.Stop();
        }
        waveIndex++;
        round.text = "ROUND: " + PlayerStart.Rounds.ToString() + "/" +  waves.Count;
    }
    void SpawnEnemy()
    {
        GameObject enemy = GetEnemy();
        enemy.transform.position = GenMap.instance.startTile.transform.position + GenMap.instance.setup;
        enemy.transform.rotation = Quaternion.identity;
        enemy.SetActive(true); 
        pooledEnemies.Remove(enemy); 
    }
    void SpawnEffect()
    {
        effectEnemySpawn.transform.position = GenMap.instance.startTile.transform.position + GenMap.instance.effectSetUp;
        effectEnemySpawn.transform.localEulerAngles = new Vector3(transform.eulerAngles.x - 90, transform.eulerAngles.y, transform.eulerAngles.z);
        effectEnemySpawn.Play();      
    }
    private GameObject GetEnemy()
    {

        for (int i = 0; i < pooledEnemies.Count; i++)
        {
            
            if (!pooledEnemies[i].activeInHierarchy)
            {              
                return pooledEnemies[i];
               
            }
  
        }
        return null;
    }

}
