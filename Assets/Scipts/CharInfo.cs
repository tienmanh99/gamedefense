using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Char", menuName = "InformationChar")]
public class CharInfo : ScriptableObject
{
    public new string name;
    public string description;

    public Sprite character;
    public float range;
    public int damge;
    public float speedAttack;
    
}
