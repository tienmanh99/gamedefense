using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Achievement : MonoBehaviour
{
    public static Achievement achieve;
    [Header("Coins")]
    public TextMeshProUGUI totalCoin;
    public TextMeshProUGUI winCoin;
    public TextMeshProUGUI lostCoin;
    private int totalcoin = 0;
    [HideInInspector]
    public int lostcoin;
    [HideInInspector]
    public int wincoin;

    [Header("Ruby")]
    public TextMeshProUGUI totalRuby;  
    public TextMeshProUGUI winRuby;
    [HideInInspector]
    public int winruby;
    private int totalruby = 0;
   
    void Awake()
    {
        if(achieve == null)
        {
            achieve = this;
        }
    }
    void Start()
    {
        if (PlayerPrefs.HasKey("TotalCoin"))
        {
            totalcoin = PlayerPrefs.GetInt("TotalCoin");
        }
        totalCoin.text = totalcoin.ToString();
        if (PlayerPrefs.HasKey("TotalRuby"))
        {
            totalruby = PlayerPrefs.GetInt("TotalRuby");
        }
        totalRuby.text = totalruby.ToString();
    }

    void Update()
    {
        winCoin.text = wincoin.ToString();
        winRuby.text = winruby.ToString();
        lostCoin.text = lostcoin.ToString();
    }
    public void changeCoin()
    {
        totalcoin += wincoin + lostcoin;
        
        PlayerPrefs.SetInt("TotalCoin", totalcoin);
        totalCoin.text = totalcoin.ToString();

        totalruby += winruby;
        PlayerPrefs.SetInt("TotalRuby", totalruby);
        totalRuby.text = totalruby.ToString();

    }
}
