using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    #region properties
    public float startSpeed = 10f;
    [HideInInspector]
    public float speed;
    private float armor = 7;
    private float magicDef = 10;
    public int worth = 50;
    public float startHealth = 100;
    private float health;
    public Image healthBar;
    public bool isDead;
    public bool isGoneToDie;

    [SerializeField] private EnemyGo moveController;
  
    #endregion
    private void Awake()
    {
        moveController = GetComponent<EnemyGo>();
    }
   
    void Start()
    {
        isDead = false;
        speed = startSpeed;
        health = startHealth;
     
    }
    #region method

    public void TakeDamge(float amount)
    {
        health -= amount;

        healthBar.fillAmount = health / startHealth;
        if (health < (startHealth / 3))
        {
            healthBar.color = Color.red;
        }
        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    void Die()
    {
        PlayerStart.Money += worth;
        WaveSpawner.EnemyAlives--;
        gameObject.SetActive(false);
        Reset();
    }
    public void Slow(float pct)
    {
        speed = startSpeed * (1f - pct);
    }
    public void Reset()
    {
        speed = startSpeed;
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        moveController.Reset();
    }
    private void OnEnable()
    {
        isGoneToDie = true;
    }
    private void OnDisable()
    {
        isGoneToDie = false;
    }
    #endregion
}
