using UnityEngine;
using System.Collections;
using UnityEngine.UI; 

public class Gamemanager : MonoBehaviour
{
    public static bool GameisOver;
    public GameObject GameOverUI;
    public GameObject completedLevelUI;
    public GameObject gameAchieveUI;
    public GameObject tutorialUI;
    private bool isFirst;
    public GameObject winEffect;
    public Button pause;

    
    void Start()
    {
        GameisOver = false;
        pause.interactable = true;
        StartCoroutine(TimeToTutorial());
    }
    // Update is called once per frame
    void Update()
    {
        if (GameisOver)
            return;
        
    }
   
    public void EndLevel()
    {
        GameisOver = true;
        pause.interactable = false;
        GameOverUI.SetActive(true);
        gameAchieveUI.SetActive(true);
        Achievement.achieve.lostcoin = 20 * (int)PlayerStart.Rounds;
        Achievement.achieve.wincoin = 0;
        Achievement.achieve.changeCoin();
    }
    public void Winlevel()
    {
        StartCoroutine(createWinEffect());
        StartCoroutine(winUI());
    }
    IEnumerator createWinEffect()
    {
        yield return new WaitForSeconds(2f);
        Instantiate(winEffect, Camera.main.transform.position, Quaternion.identity);
    }
    IEnumerator winUI()
    {
        yield return new WaitForSeconds(3f);
        completedLevelUI.SetActive(true);
        pause.interactable = false;
        gameAchieveUI.SetActive(true);
        Achievement.achieve.wincoin = 500 + 10 * (int)PlayerStart.Rounds;
        Achievement.achieve.lostcoin = 0;
        Achievement.achieve.winruby = 5;
        Achievement.achieve.changeCoin();
    }
    public void Tutorial()
    {

        Toggle();
        
    }
    public void Toggle()
    {
        tutorialUI.SetActive(!tutorialUI.activeSelf);
        if (tutorialUI.activeSelf)
        {
            Time.timeScale = 0f;
        }
        if (tutorialUI.activeSelf == false)
        {
            Time.timeScale = 1f;
        }
    }
    IEnumerator TimeToTutorial()
    {
        if (PlayerPrefs.HasKey("genmaplist"))
        {
            tutorialUI.SetActive(false);

        }
        else
        {
            tutorialUI.SetActive(true);
            yield return new WaitForSeconds(1f);
            Time.timeScale = 0f;

        }
    }

}
