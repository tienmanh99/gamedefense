using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(Enemy))]
public class EnemyGo : MonoBehaviour
{
    WarningControl warning;
    private GameObject target;
   
    private int wavepointIndex = 0;
    private Enemy enemy;
    public static int curLive;
    public static int preLive;
    public List<GameObject> path;
    public Transform partToBase;
    public float turnSpeed = 5f;
    
    private void Start()
    {
        path = GenMap.instance.waylane;
        enemy = GetComponent<Enemy>();
        target = path[0];
        curLive = PlayerStart.Lives;
    }
    void Update()
    {
        Vector3 dir = target.transform.position - transform.position;
        transform.Translate(dir.normalized * enemy.speed * Time.deltaTime, Space.World);
        LookOnWayPoint();
        if (Vector3.Distance(transform.position, target.transform.position) <= 0.2f)
        {
            GetNextPoint();
        }
        enemy.speed = enemy.startSpeed;
    }
    void GetNextPoint()
    {
        if (wavepointIndex >= path.Count - 1)
        {
            Endpath();
         
            return;
        }
        wavepointIndex++;
        
        target = path[wavepointIndex];
    }
    void Endpath()
    {
        curLive = PlayerStart.Lives;
        PlayerStart.Lives--;
        preLive = curLive;
        curLive = PlayerStart.Lives;
        WaveSpawner.EnemyAlives--;
        
        gameObject.SetActive(false);
    }
    void LookOnWayPoint()
    {
        if (target == null)
            return;
        Vector3 direction = transform.position - target.transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(partToBase.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToBase.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }
    public void Reset()
    {
        wavepointIndex = 0;
        target = path[0];
        enemy.speed = enemy.startSpeed;
    }
    

}
    
