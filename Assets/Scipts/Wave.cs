using UnityEngine;

[System.Serializable]

public class Wave
{
    public GameObject enemy;
    public string name;
    public int count;
    public float rate;
}
