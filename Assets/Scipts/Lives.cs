using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class Lives : MonoBehaviour
{
    public Text textLives;

    void Update()
    {
        textLives.text = PlayerStart.Lives.ToString();
    }

}
