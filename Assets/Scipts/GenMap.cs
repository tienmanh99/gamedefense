using System.Collections.Generic;
using UnityEngine;

public class GenMap : MonoBehaviour
{
    [Header("General")]
    public GameObject Map;
    public static GenMap instance;
    public int widthmap;
    public int heighmap;
    public string mapID;
    public Vector3 offSet;
    public Vector3 setup;
    public Vector3 effectSetUp;
    [Header("gameObj")]
    public GameObject SpawnPoint;
    public GameObject Endpoind;
    public GameObject wayPrefabs;
    public GameObject startTile;
    public GameObject endTile;
    public GameObject waypoints;
    public SaveManager savemanage;
    GameObject curTiles;
    private int curIndex;
    private int nextIndex;
    private bool reachedX = false;
    private bool reachedZ = false;
    private bool goleft = false;
    private bool godown = false;
    protected GameObject pointLeft;
    protected GameObject pointRight;
    [HideInInspector] public List<GameObject> maptiles = new List<GameObject>();
    private List<GameObject> way = new List<GameObject>();
    [HideInInspector] public List<GameObject> waylane = new List<GameObject>();
   

    void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        List<GameObject> waylane = new List<GameObject>();
        if (PlayerPrefs.HasKey("genmap"))
        {
            savemanage.Load(mapID);
        }
        else
        {
            savemanage.Init();
            genMap();
            savemanage.Save();
       
        }

    }
    // get bottom row of the map
    private List<GameObject> getBottomEdge()
    {
        List<GameObject> edgemap = new List<GameObject>();
        for (int i = (widthmap / 4) * ((heighmap / 4) - 2); i < (widthmap / 4) * ((heighmap / 4) - 1); i++)
        {
            edgemap.Add(maptiles[i]);
        }

        return edgemap;
    }
    //get top row of the map
    private List<GameObject> getTopEdge()
    {
        List<GameObject> edgemap = new List<GameObject>();
        for (int i = 1; i < ((widthmap / 4) - 1); i++)
        {

            edgemap.Add(maptiles[i]);
        }
        return edgemap;
    }
    private void moveLeft()
    {
        way.Add(curTiles);
        curIndex = maptiles.IndexOf(curTiles);
        nextIndex = curIndex - 1;
        curTiles = maptiles[nextIndex];
    }
    private void moveRight()
    {
        way.Add(curTiles);
        curIndex = maptiles.IndexOf(curTiles);
        nextIndex = curIndex + 1;
        curTiles = maptiles[nextIndex];
    }
    private void moveDown()
    {
        way.Add(curTiles);
        curIndex = maptiles.IndexOf(curTiles);
        nextIndex = curIndex + (widthmap / 4);
        curTiles = maptiles[nextIndex];
    }
    private void goDown()
    {
        reachedZ = false;
        while (!godown)
        {

            if (curTiles.transform.position.x < endTile.transform.position.x)
            {
                moveDown();
            }
            else
            {
                godown = true;
            }

        }
    }
    private void wayDown()
    {
        goleft = false;
        reachedZ = false;
        int downCount = 0;
        while (!reachedX)
        {

            downCount++;
            if (curTiles.transform.position.x < endTile.transform.position.x)
            {
                moveDown();
            }
            if (downCount == 3)
            {
                reachedX = true;
            }
            if (curTiles.transform.position.x >= endTile.transform.position.x)
            {
                reachedX = true;
            }

        }
    }
    private void goDownhaftMap()
    {
        goleft = false;
        reachedZ = false;
        int downCount = 0;
        while (!reachedX)
        {

            downCount++;
            if (curTiles.transform.position.x < endTile.transform.position.x)
            {
                moveDown();
            }
            if (downCount == (heighmap / 4) / 2)
            {
                reachedX = true;
            }
            if (curTiles.transform.position.x >= endTile.transform.position.x)
            {
                reachedX = true;
            }

        }
    }
    private void moveLeftAndRight()
    {
        goleft = false;
        int loopCount = 0;
        while (!reachedZ)
        {
            loopCount++;
            if (loopCount > 1000)
            {
                break;
            }

            else if (curTiles.transform.position.z > endTile.transform.position.z)
            {
                moveLeft();
            }
            else if (curTiles.transform.position.z < endTile.transform.position.z)
            {
                moveRight();
            }
            else
            {
                reachedZ = true;
            }
        }
    }
    private void goRight()
    {
        reachedX = false;
        while (!goleft)
        {
            if (curTiles.transform.position.z < pointRight.transform.position.z)
            {
                moveRight();
            }
            else
            {
                goleft = true;
            }
        }
        wayDown();
    }
    private void goLeft()
    {
        reachedX = false;
        while (!goleft)
        {
            if (curTiles.transform.position.z > pointLeft.transform.position.z)
            {
                moveLeft();
            }
            else
            {
                goleft = true;
            }
        }
        wayDown();
    }
    // gen map
    public void genMap()
    {
        savemanage.cachedPath.Clear();
        int y = 0;
        for (int x = 0; x < heighmap; x += 4)
        {
            for (int z = 0; z < widthmap; z += 4)
            {
                GameObject newMap = (GameObject)Instantiate(Map);
                maptiles.Add(newMap);
                newMap.transform.position = new Vector3(x, y, z);
            }
        }
        List<GameObject> botEdgeTiles = getBottomEdge();
        List<GameObject> topEdgeTile = getTopEdge();
        int rand1 = Random.Range(1, (widthmap / 4) - 2);
        int rand2 = Random.Range(1, (widthmap / 4) - 2);
        int rand3 = Random.Range(1, 4);

        // find startTile and endTile
        startTile = topEdgeTile[rand1];
        endTile = botEdgeTiles[rand2];
        int posRight = ((widthmap / 4) - 3);
        int posLeft = 0;
        pointRight = topEdgeTile[posRight];
        pointLeft = topEdgeTile[posLeft];
        curTiles = startTile;

        //raw way
        switch (rand3)
        {
            case 1:
                moveDown();
                for (int i = 0; i < ((heighmap / 4) / 6); i++)
                {
                    goLeft();
                    goRight();
                }
                moveLeftAndRight();
                goDown();
                break;
            case 2:
                if(rand2 > (widthmap / 4) / 2)
                {
                    goDownhaftMap();
                    goLeft();
                    goDown();
                    moveLeftAndRight();
                }
                else
                {
                    goDownhaftMap();
                    goRight();
                    goDown();
                    moveLeftAndRight();
                }
                break;
            case 3:
                if (rand2 > (widthmap / 4) / 2)
                {
                    moveDown();
                    goLeft();
                    goDown();
                    moveLeftAndRight();
                }
                else
                {
                    moveDown();
                    goRight();
                    goDown();
                    moveLeftAndRight();
                }
                break;
               
        }

        way.Add(endTile);
        foreach (GameObject tile in way)
        {
            GameObject pathlane = Instantiate(wayPrefabs, tile.transform.position, Quaternion.identity);
            GameObject pathLane = Instantiate(waypoints, tile.transform.position + setup, Quaternion.identity);
            waylane.Add(pathLane);
            savemanage.cachedPath.Add(tile.transform.position);
        }
        
        GameObject str = Instantiate(SpawnPoint, startTile.transform.position + offSet, Quaternion.identity);
        GameObject ed = Instantiate(Endpoind, endTile.transform.position + offSet, Quaternion.identity);
       
        Debug.Log(savemanage.cachedPath.Count);
        
        savemanage.OnMapGenerate?.Invoke();
        Debug.Log(savemanage.OnMapGenerate);
    }

}

