using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    #region Properties
    private const float TIME_COMBO = 1.5f;
    private float lastTimeAttack;
    private int countStack;
    private Transform target;
    private Enemy targetEnemy;
    private float animSpeed;

    [Header("General")]
    public float range = 12f;
    public string tag;
    public bool useLaze = false;
    public bool useSword = false;
    public bool useHammer = false;

    [Header("Use bullet(default)")]
    public GameObject slashPrefabs;
    public float countBullDown = 0f;
    public float attackRate = 1f;
    public float startAnimSpeed;

    [Header("Use laze (default)")]
    public LineRenderer linerender;
    public ParticleSystem impactEffect;
    public Light lightEffect;
    public int takeDamgeOverTime = 30;
    public float slowPct = .5f;

    [Header("Set up")]
    public string enemyTag = "Enemy";
    public Transform partToBase;
    public float turnSpeed = 10f;
    public Transform attackPoint;
    public Animator animator;

    #endregion

    void Start()
    {
       
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
        animator.enabled = true;
        countStack = 0;
        animSpeed = startAnimSpeed;
    }

    private void UpdateTarget()
    {
        target = null;
        targetEnemy = null;
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
           
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
   
            }
            if (nearestEnemy != null && shortestDistance <= range)
            {

                targetEnemy = nearestEnemy.GetComponent<Enemy>();
              
                if (!targetEnemy.isGoneToDie)
                {
                    target = null;
                    targetEnemy = null;
                }
                else
                {
                    target = nearestEnemy.transform;
                }
                
            }
            else
            {
                target = null;
            }
            


        }
    }
    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("AnimSpeed", animSpeed);
        if (target == null)
        {
            if(targetEnemy == null)
            if (useLaze)
            {
                if (linerender.enabled)
                {
                    linerender.enabled = false;
                    impactEffect.Stop();
                    lightEffect.enabled = false;
                }
                    
            }
            animator.SetBool("isAttack", false);
            return;
        }
        
        LockOnEnemy();
        if (useLaze)
        {
            animator.SetBool("isAttack", true);
            Laser();
        }
        else
        {
            if (countBullDown <= 0f)
            {
                animator.SetBool("isAttack", true);
                countBullDown = 1 / attackRate;
            }
            countBullDown -= Time.deltaTime;
        }
        if (countStack >= 5)
        {
            animSpeed = startAnimSpeed + 1;
            
        }
        else
        {
            animSpeed = startAnimSpeed;
        }
    }

    void Laser()
    {
        if (target == null)
            return;
        if (targetEnemy == null)
            return;
        targetEnemy.TakeDamge(takeDamgeOverTime * Time.deltaTime);
        targetEnemy.Slow(slowPct);
        if (!linerender.enabled)
        {
            linerender.enabled = true;
            impactEffect.Play();
            lightEffect.enabled = true;
        }

        linerender.SetPosition(0, attackPoint.position);
        linerender.SetPosition(1, target.position);

        Vector3 dir = attackPoint.position - target.position;
        impactEffect.transform.position = target.position + dir.normalized;
        impactEffect.transform.rotation = Quaternion.LookRotation(dir);

    }
    void LockOnEnemy()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToBase.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToBase.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }
    public void Attack()
    {
        if (useSword)
        {
            if (target == null)
                return;
            GameObject bulletGo = BulletPooler.instance.GetPooledObj(tag);
            if (bulletGo != null)
            {
                bulletGo.transform.position = attackPoint.position;
                bulletGo.transform.rotation = attackPoint.rotation;
                bulletGo.SetActive(true);
            }
            
            Bullet bullet = bulletGo.GetComponent<Bullet>();
            CountCombo();
            if (bullet != null)
            {
                bullet.Seak(target);
            }

        }
        else if (useHammer)
        {
            if(target == null)
                return;
            GameObject bulletGo = BulletPooler.instance.GetPooledObj(tag);
            if (bulletGo != null)
            {
                bulletGo.transform.position = target.position;
                bulletGo.transform.rotation = target.rotation;
                bulletGo.SetActive(true);
            }
            //GameObject bulletGo = (GameObject)Instantiate(slashPrefabs, target.position, Quaternion.identity);
            Bullet bullet = bulletGo.GetComponent<Bullet>();
            CountCombo();
            if (bullet != null)
            {
                bullet.Seak(target);
            }
        }
        
    }
    //void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireSphere(transform.position, range);
    //}

    //xac dinh type damge
    //truyen type damage den bullet
    //tach thanh cac class rieng 
    private void CountCombo()
    {
        float timeBetweenAttack = Time.time - lastTimeAttack;
        if(timeBetweenAttack <= TIME_COMBO)
        {
            countStack += 1;

        }
        else
        {
            countStack = 0;
        }
        lastTimeAttack = Time.time;
    }
    
}
